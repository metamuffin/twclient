#![feature(random)]
use anyhow::Result;
use client::Client;
use std::sync::Arc;
use window::WindowState;
use winit::event_loop::EventLoop;

pub mod client;
pub mod renderer;
pub mod skin_manager;
pub mod window;

fn main() -> Result<()> {
    env_logger::init_from_env("LOG");

    let client = Arc::new(Client::new().unwrap());
    let evloop = EventLoop::new()?;
    evloop.run_app(&mut WindowState::new(client))?;

    Ok(())
}
