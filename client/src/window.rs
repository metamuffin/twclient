use std::sync::Arc;

use crate::{client::Client, renderer::Renderer};
use log::warn;
use winit::{
    application::ApplicationHandler,
    event::WindowEvent,
    event_loop::ActiveEventLoop,
    window::{Window, WindowAttributes, WindowId},
};

pub struct WindowState {
    temp_client: Option<Arc<Client>>,
    window: Option<(Window, Renderer<'static>)>,
}

impl WindowState {
    pub fn new(client: Arc<Client>) -> Self {
        Self {
            window: None,
            temp_client: Some(client),
        }
    }
}
impl ApplicationHandler for WindowState {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        let window = event_loop
            .create_window(WindowAttributes::default().with_maximized(true))
            .unwrap();
        let renderer = Renderer::new(
            unsafe { std::mem::transmute(&window) },
            self.temp_client.take().unwrap(),
        )
        .unwrap();
        self.window = Some((window, renderer))
    }

    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        _window_id: WindowId,
        event: WindowEvent,
    ) {
        if let Some((_win, ren)) = &mut self.window {
            match event {
                WindowEvent::CloseRequested => {
                    event_loop.exit();
                }
                WindowEvent::Resized(size) => {
                    ren.resize(size);
                }
                WindowEvent::RedrawRequested => {
                    if let Err(e) = ren.redraw() {
                        warn!("{e:?}")
                    }
                }
                _ => (),
            }
        }
    }
}

impl Drop for WindowState {
    fn drop(&mut self) {
        if let Some((win, ren)) = self.window.take() {
            drop(ren);
            drop(win);
        }
    }
}
